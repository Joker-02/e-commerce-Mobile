const { Category } = require("../models/category");
const express = require("express");
const routers = express.Router();
//GET CATEGORIES
routers.get(`/`, async (req, res) => {
  try {
    const categoryList = await Category.find();
    if (!categoryList) {
      res.status(404).json({ message: "Category not found", success: false });
    }
    res.status(200).send(categoryList);
  } catch (e) {
    return res.status(400).json({ message: e, success: false });
  }
});
//GET category/id
routers.get("/:id", async (req, res) => {
  try {
    const category = await Category.findById(req.params.id);
    if (!category) {
      return res
        .status(404)
        .json({ message: "Category not found", success: false });
    }
    res.status(200).send(category);
  } catch (e) {
    return res.status(400).json({ message: e, success: false });
  }
});
//POST Category
routers.post("/", async (req, res) => {
  try {
    let category = new Category({
      name: req.body.name,
      icon: req.body.icon,
      color: req.body.color,
    });
    category = await category.save();
    if (!category) {
      return res.status(404).send("Can't add category");
    }
    res.send(category);
  } catch (e) {
    return res.status(400).json({ message: e, success: false });
  }
});
//DELETE category/id
routers.delete("/:id", async (req, res) => {
  try {
    let category = await Category.findByIdAndRemove(req.params.id);
    if (!category) {
      return res
        .status(404)
        .json({ message: "can't delete category", success: true });
    }
    res.send(category);
  } catch (e) {
    return res.status(400).json({ message: e, success: false });
  }
});
//PUT /category/:id
routers.put("/:id", async (req, res) => {
  try {
    const category = await Category.findByIdAndUpdate(
      req.params.id,
      {
        name: req.body.name,
        icon: req.body.icon,
        color: req.body.color,
      },
      { new: true }
    );
    if (!category)
      return res.status(400).json({
        message: "Category not found cannot be updated",
        success: false,
      });
    res.status(200).send(category);
  } catch (e) {
    return res.status(400).json({ message: e, success: false });
  }
});

module.exports = routers;
