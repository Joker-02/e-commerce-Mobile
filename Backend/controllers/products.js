const { Product } = require("../models/product");
const { Category } = require("../models/category");
const express = require("express");
const routers = express.Router();
const multer = require("multer");
const FILE_TYPE_MAP = {
  "image/png": "png",
  "image/jpeg": "jpeg",
  "image/jpg": "jpg",
};
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    const isvalid = FILE_TYPE_MAP[file.mimetype];
    let uploadError = new Error("Invalid image type");
    if (isvalid) uploadError = null;
    cb(uploadError, "public/uploads");
  },
  filename: function (req, file, cb) {
    const fileName = file.originalname.split(" ").join("-");
    const extension = FILE_TYPE_MAP[file.mimetype];
    cb(null, `${fileName}-${Date.now()}.${extension}`);
  },
});

const upload = multer({ storage: storage });
//GET PRODUCTS
routers.get(`/`, async (req, res) => {
  try {
    let filter = {};
    if (req.query.categories) {
      filter = { category: req.query.categories.split(",") };
    }
    console.log(filter);
    let productList = await Product.find(filter).populate("category");
    if (!productList) {
      res.status(404).json({ message: "Product not found", success: false });
    }
    res.send(productList);
  } catch (err) {
    console.log(err.message);
    res.status(500).json({ message: err.message });
  }
});
//GET PRODUCT/:id
routers.get("/:id", async (req, res) => {
  try {
    const product = await Product.findById(req.params.id).populate("category");
    if (!product) {
      res.status(404).json({ message: "Product not found", success: false });
    }
    res.status(200).send(product);
  } catch (err) {
    console.log(err.message);
  }
});
//POST PRODUCTS
routers.post(`/`, upload.single("image"), async (req, res) => {
  try {
    let category = await Category.findById(req.body.category);
    if (!category) return res.status(400).send("Invalid category");
    const file = req.file;
    if (!file) return res.status(404).send("There no image attach");
    const fileName = req.file.filename;
    const basePath = `${req.protocol}://${req.get("host")}/public/uploads/`;
    let product = new Product({
      name: req.body.name,
      description: req.body.description,
      richDescription: req.body.richDescription,
      image: `${basePath}${fileName}`,
      brand: req.body.brand,
      price: req.body.price,
      category: req.body.category,
      countInStock: req.body.countInStock,
      rating: req.body.rating,
      numReviews: req.body.numReviews,
      isFeatured: req.body.isFeatured,
    });
    product = await product.save();
    if (!product) {
      return res.status(404).send("The product was not created");
    }
    res.send(product);
  } catch (e) {
    console.log(e.message);
    res.status(400).json({ message: e, success: false });
  }
});
//PUt UPLOAD GALARY
routers.put("/gallery-image/:id", upload.array("images"), async (req, res) => {
  try {
    const basePath = `${req.protocol}://${req.get("host")}/public/uploads/`;
    const files = req.files;
    let imagesPath = [];
    if (files) {
      files.map((file) => {
        imagesPath.push(`${basePath}${file.filename}`);
      });
    }
    const product = await Product.findByIdAndUpdate(
      req.params.id,
      {
        images: imagesPath,
      },
      { new: true }
    );
    if (!product) {
      return res
        .status(404)
        .json({ message: "Product not found", success: false });
    }
    res.send(product);
  } catch (err) {
    console.error(err.message);
    return res.status(400).json({ message: err.message, success: false });
  }
});
//PUT /products/:id
routers.put("/:id", async (req, res) => {
  try {
    let category = await Category.findById(req.body.category);
    if (!category) return res.status(400).send("Invalid category");

    let product = await Product.findByIdAndUpdate(
      req.params.id,
      {
        name: req.body.name,
        description: req.body.description,
        richDescription: req.body.richDescription,
        image: req.body.image,
        brand: req.body.brand,
        price: req.body.price,
        category: req.body.category,
        countInStock: req.body.countInStock,
        rating: req.body.rating,
        numReviews: req.body.numReviews,
        isFeatured: req.body.isFeatured,
      },
      { new: true }
    );
    if (!product)
      return res.status(400).json({
        message: "Product not found cannot be updated",
        success: false,
      });
    res.status(200).send(product);
  } catch (e) {
    return res.status(400).json({ message: e, success: false });
  }
});
//DELETE product/id
routers.delete("/:id", async (req, res) => {
  try {
    let product = await Product.findByIdAndRemove(req.params.id);
    if (!product) {
      return res
        .status(404)
        .json({ message: "can't delete product", success: true });
    }
    res.send(product);
  } catch (e) {
    return res.status(400).json({ message: e, success: false });
  }
});
//GET COUNT products
routers.get("/get/count", async (req, res) => {
  try {
    let productCount = await Product.countDocuments();
    if (!productCount)
      res.status(400).json({ message: "Product not found", success: false });
    res.send({
      count: productCount,
    });
  } catch (e) {
    return res.status(400).json({ message: e.message, success: false });
  }
});
//GET FEATURE Product
routers.get("/get/featured/:count", async (req, res) => {
  try {
    const count = req.params.count ? req.params.count : 0;

    let products = await Product.find({ isFeatured: true }).limit(+count);
    if (!products)
      res.status(400).json({ message: "Product not found", success: false });
    res.send(products);
  } catch (e) {
    return res.status(400).json({ message: e.message, success: false });
  }
});

module.exports = routers;
