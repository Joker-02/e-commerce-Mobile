const { Order } = require("../models/order");
const { OrderItem } = require("../models/order-item");
const express = require("express");
const routers = express.Router();
//GET Request order
routers.get(`/`, async (req, res) => {
  try {
    const orderList = await Order.find()
      .populate("user", "name email phone")
      .sort({ dateOrdered: -1 })
      .populate({
        path: "orderItems",
        populate: { path: "product", populate: "category" },
      });
    if (!orderList) {
      res.status(404).json({ message: "Order not found", success: false });
    }
    res.send(orderList);
  } catch (e) {
    console.log(e.message);
  }
});
//GET Request:id
routers.get("/:id", async (req, res) => {
  try {
    const order = await Order.findById(req.params.id)
      .populate("user", "name email phone")
      .populate({
        path: "orderItems",
        populate: { path: "product", populate: "category" },
      });
    if (!order) {
      return res
        .status(404)
        .json({ message: "order not found", success: false });
    }
    res.status(200).send(order);
  } catch (e) {
    return res.status(400).json({ message: e, success: false });
  }
});
//POST Request Order
routers.post("/", async (req, res) => {
  try {
    const orderItemsIds = Promise.all(
      req.body.orderItems.map(async (orderItem) => {
        let newOrderItem = new OrderItem({
          quantity: orderItem.quantity,
          product: orderItem.product,
        });
        newOrderItem = await newOrderItem.save();
        return newOrderItem._id;
      })
    );

    const orderItemsIdsResolved = await orderItemsIds;
    console.log(orderItemsIdsResolved);
    const totalPrice = await Promise.all(
      orderItemsIdsResolved.map(async (orderItemId) => {
        const orderItem = await OrderItem.findById(orderItemId).populate(
          "product",
          "price"
        );

        const totalPrice = orderItem.product.price * orderItem.quantity;

        return totalPrice;
      })
    );

    const totalPrices = totalPrice.reduce((a, b) => a + b, 0);
    let order = new Order({
      orderItems: orderItemsIdsResolved,
      shippingAddress1: req.body.shippingAddress1,
      shippingAddress2: req.body.shippingAddress2,
      city: req.body.city,
      zip: req.body.zip,
      country: req.body.country,
      phone: req.body.phone,
      status: req.body.status,
      totalPrice: totalPrices,
      user: req.body.user,
    });
    order = await order.save();
    if (!order) {
      return res.status(404).send("Can't add order");
    }
    res.send(order);
  } catch (e) {
    return res.status(400).json({ message: e, success: false });
  }
});
//PUT /order/:id
routers.put("/:id", async (req, res) => {
  try {
    const order = await Order.findByIdAndUpdate(
      req.params.id,
      {
        status: req.body.status,
      },
      { new: true }
    )
      .populate("user", "name email phone")
      .populate({
        path: "orderItems",
        populate: { path: "product", populate: "category" },
      });
    if (!order)
      return res.status(400).json({
        message: "order not found cannot be updated",
        success: false,
      });
    res.status(200).send(order);
  } catch (e) {
    return res.status(400).json({ message: e, success: false });
  }
});
//DELETE Order
routers.delete("/:id", async (req, res) => {
  try {
    let order = await Order.findByIdAndRemove(req.params.id);

    if (!order) {
      return res
        .status(404)
        .json({ message: "can't delete order", success: true });
    }
    for (i = 0; i < order.orderItems.length; i++) {
      let orderItem = await OrderItem.findByIdAndRemove(order.orderItems[i]);
      if (!orderItem)
        return res
          .status(404)
          .json({ message: "order item not found", success: false });
    }
    res.status(200).json({ message: "order deleted", success: true });
  } catch (e) {
    return res.status(400).json({ message: e, success: false });
  }
});
//GET Total Sales
routers.get("/get/totalsales", async function (req, res) {
  const totalSales = await Order.aggregate([
    { $group: { _id: null, totalSales: { $sum: "$totalPrice" } } },
  ]);
  if (!totalSales) {
    return res.status(400).send("the order sales was not generated");
  }
  res.send({ totalSales: totalSales.pop().totalSales });
});
//GET COUNT products
routers.get("/get/count", async (req, res) => {
  try {
    let orderCount = await Order.countDocuments();
    if (!orderCount)
      res.status(400).json({ message: "Product not found", success: false });
    res.send({
      count: orderCount,
    });
  } catch (e) {
    return res.status(400).json({ message: e.message, success: false });
  }
});
module.exports = routers;
