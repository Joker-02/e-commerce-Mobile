const { User } = require("../models/user");
const express = require("express");
const routers = express.Router();
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
//GET USER
routers.get(`/`, async (req, res) => {
  try {
    const userList = await User.find().select("-passwordHash");
    if (!userList) {
      res.status(404).json({ message: "user not found", success: false });
    }
    res.send(userList);
  } catch (e) {
    console.log(e.message);
    return res.status(400).json({ message: e.message, success: false });
  }
});
//GET USER/:id
routers.get("/:id", async function (req, res) {
  try {
    const user = await User.findById(req.params.id).select("-passwordHash");
    if (!user) {
      return res
        .status(404)
        .json({ message: "user not found", success: false });
    }
    res.send(user);
  } catch (err) {
    console.log(err.message);
    return res.status(500).json({ message: err.message, success: false });
  }
});
//POST USER
routers.post("/", async (req, res) => {
  try {
    let user = new User({
      name: req.body.name,
      email: req.body.email,
      color: req.body.color,
      passwordHash: bcrypt.hashSync(req.body.password, 10),
      phone: req.body.phone,
      isAdmin: req.body.isAdmin,
      apartment: req.body.apartment,
      zip: req.body.zip,
      city: req.body.city,
      country: req.body.country,
      street: req.body.street,
    });
    user = await user.save();

    if (!user) {
      return res
        .status(404)
        .json({ message: "user cannot be create", success: false });
    }
    res.send(user);
  } catch (e) {
    console.log(e.message);
    return res.status(500).json({ message: e.message, success: false });
  }
});
//POST USER Login
routers.post("/login", async (req, res) => {
  try {
    const user = await User.findOne({ email: req.body.email });
    const secret = process.env.SECRET_KEY;
    if (!user) {
      return res.status(404).json({ message: "user dont exist" });
    }
    if (user && bcrypt.compareSync(req.body.password, user.passwordHash)) {
      const token = jwt.sign(
        {
          userId: user.id,
          isAdmin: user.isAdmin,
        },
        secret,
        { expiresIn: "1d" }
      );
      res.status(200).send({ user: user.email, token: token });
    } else {
      res.status(401).send("password is incorrect");
    }
  } catch (e) {
    console.log(e.message);
  }
});
//USER Registration
//POST USER
routers.post("/register", async (req, res) => {
  try {
    let user = new User({
      name: req.body.name,
      email: req.body.email,
      color: req.body.color,
      passwordHash: bcrypt.hashSync(req.body.password, 10),
      phone: req.body.phone,
      isAdmin: req.body.isAdmin,
      apartment: req.body.apartment,
      zip: req.body.zip,
      city: req.body.city,
      country: req.body.country,
      street: req.body.street,
    });
    user = await user.save();

    if (!user) {
      return res
        .status(404)
        .json({ message: "user cannot be create", success: false });
    }
    res.send(user);
  } catch (e) {
    console.log(e.message);
    return res.status(500).json({ message: e.message, success: false });
  }
});
//COUNT THE USER
routers.get("/get/count", async (req, res) => {
  try {
    let userCount = await User.countDocuments();
    if (!userCount)
      res.status(400).json({ message: "Product not found", success: false });
    res.send({
      count: userCount,
    });
  } catch (e) {
    return res.status(400).json({ message: e.message, success: false });
  }
});
//DELETE product/id
routers.delete("/:id", async (req, res) => {
  try {
    let user = await User.findByIdAndRemove(req.params.id);
    if (!user) {
      return res
        .status(404)
        .json({ message: "can't delete product", success: true });
    }
    res.send(user);
  } catch (e) {
    return res.status(400).json({ message: e, success: false });
  }
});
module.exports = routers;
