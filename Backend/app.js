const express = require("express");
const app = express();
const morgan = require("morgan");
const mongoose = require("mongoose");
require("dotenv/config");
const bodyParser = require("body-parser");
const api = process.env.API_URL;
//routers
const productsRouter = require("./controllers/products");
const categoryRouter = require("./controllers/categories");
const orderRouter = require("./controllers/orders");
const userRouter = require("./controllers/users");
const cors = require("cors");
const authJwt = require("./helpers/jwt");
const errorHandler = require("./helpers/error-handler");
app.use(cors());
app.options("*", cors());
//middleware
app.use(bodyParser.json());
app.use(morgan("tiny"));
app.use(authJwt());
app.use(errorHandler);
app.use("/public/uploads", express.static(__dirname + "/public/uploads"));
//controllers
app.use(`${api}/products`, productsRouter);
app.use(`${api}/categories`, categoryRouter);
app.use(`${api}/orders`, orderRouter);
app.use(`${api}/users`, userRouter);
//connect to server
mongoose
  .connect(process.env.CONNECTION_STRING, {
    dbName: "Ptaslek1",
  })
  .then(() => {
    console.log("Connected");
  })
  .catch((err) => {
    console.log(err);
  });
//listening
app.listen(3000, () => {
  //console.log(api + "/products");
  console.log("listening http://localhost:3000");
});
